package com.order_bricks.service.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.order_bricks.service.JSONBUserType;
import com.order_bricks.service.model.OrderBricksRequest;

@Entity
@Table(name = "order_details")
@TypeDef(name = "jsonb", typeClass = JSONBUserType.class, parameters = {
		@Parameter(name = JSONBUserType.CLASS, value = "com.order_bricks.service.model.OrderBricksRequest") })
public class OrderDetailEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_id_sequence")
	@SequenceGenerator(name = "order_id_sequence", sequenceName = "order_id_sequence", initialValue = 1,
	allocationSize = 1)
	@Column(name = "order_id")
	private int orderId;

	@Column(name = "transaction_date")
	private Timestamp transactionDate;

	@Column(name = "order_date")
	private Timestamp orderDate;

	@Column(name = "orderdelivery_date")
	private Timestamp orderDeliveryDate;

	@Column(name = "orderdispatched_date")
	private Timestamp orderDispatchedDate;

	@Column(name = "total_amount")
	private Double totalAmount;

	@Column(name = "payload")
	@Type(type = "jsonb")
	private OrderBricksRequest orderBricksRequest;

	@Column(name = "created_date")
	private Timestamp createdDate;

	@Column(name = "updated_date")
	private Timestamp updatedDate;

	@Column(name = "status")
	private String status;
	
	

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public Timestamp getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Timestamp getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Timestamp orderDate) {
		this.orderDate = orderDate;
	}

	public Timestamp getOrderDeliveryDate() {
		return orderDeliveryDate;
	}

	public void setOrderDeliveryDate(Timestamp orderDeliveryDate) {
		this.orderDeliveryDate = orderDeliveryDate;
	}

	public Timestamp getOrderDispatchedDate() {
		return orderDispatchedDate;
	}

	public void setOrderDispatchedDate(Timestamp orderDispatchedDate) {
		this.orderDispatchedDate = orderDispatchedDate;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public OrderBricksRequest getOrderBricksRequest() {
		return orderBricksRequest;
	}

	public void setOrderBricksRequest(OrderBricksRequest orderBricksRequest) {
		this.orderBricksRequest = orderBricksRequest;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

}
