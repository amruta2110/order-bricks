package com.order_bricks.service.entity;

import org.hibernate.SessionFactory;

import io.dropwizard.hibernate.AbstractDAO;

public class OrderDAO extends AbstractDAO<OrderDetailEntity> {

	public OrderDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public void createOrder(OrderDetailEntity orderDetailEntity) {
		currentSession().persist(orderDetailEntity);
		currentSession().flush();
	}

	public OrderDetailEntity getOrderDetails(int orderId) {
		return (OrderDetailEntity) currentSession().get(OrderDetailEntity.class, orderId);
	}

	public void updateOrder(OrderDetailEntity orderDetailEntity) {
		currentSession().update(orderDetailEntity);
		currentSession().flush();
	}
}
