/**
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.order_bricks.service;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.internal.util.ReflectHelper;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A {@link UserType} that persists objects as JSONB.
 * <p>
 * Unlike the default JPA object mapping, {@code JSONBUserType} can also be used
 * for properties that do not implement {@link Serializable}.
 * <p>
 * Users intending to use this type for mutable non-<code>Collection</code>
 * objects should override {@link #deepCopyValue(Object)} to correctly return a
 * <u>copy</u> of the object.
 */
public class JSONBUserType extends CollectionUserType implements
    ParameterizedType {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private static final String JSONB_TYPE = "jsonb";
  public static final String CLASS = "CLASS";
  private Class returnedClass;
  private static final Logger logger = LoggerFactory.getLogger(JSONBUserType.class);

  public Class<Object> returnedClass() {
    return Object.class;
  }

  public int[] sqlTypes() {
    return new int[]{Types.JAVA_OBJECT};
  }



  @Override
  protected Object deepCopyValue(Object value) {
    return value;
  }

  public void setParameterValues(Properties parameters) {
    final String clazz = (String) parameters.get(CLASS);
    try {
      returnedClass = ReflectHelper.classForName(clazz);
    } catch (ClassNotFoundException e) {
    	logger.error("error occurred in setParameterValues",e);
    }
  }

public Object nullSafeGet(ResultSet resultSet, String[] names, SharedSessionContractImplementor arg2, Object arg3)
		throws HibernateException, SQLException {
	try {
	      final String json = resultSet.getString(names[0]);
	      return json == null
	          ? null
	          : MAPPER.readValue(json, returnedClass);
	    } catch (IOException ex) {
	      throw new HibernateException(ex);
	    }
}

public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor arg3)
		throws HibernateException, SQLException {
	try {
	      final String json = value == null
	          ? null
	          : MAPPER.writeValueAsString(value);
	      // otherwise PostgreSQL won't recognize the type
	      PGobject pgo = new PGobject();
	      pgo.setType(JSONB_TYPE);
	      pgo.setValue(json);
	      st.setObject(index, pgo);
	    } catch (JsonProcessingException ex) {
	      throw new HibernateException(ex);
	    }	
}

}
