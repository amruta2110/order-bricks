package com.order_bricks.service;

import com.order_bricks.service.config.OrderBricksServiceConfig;
import com.order_bricks.service.config.OrderBricksServiceConfigProvider;
import com.order_bricks.service.entity.OrderDAO;
import com.order_bricks.service.entity.OrderDetailEntity;
import com.order_bricks.service.resource.OrderBricksResource;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class OrderBricksApplication extends Application<OrderBricksServiceConfig> {

	public static void main(String[] args) throws Exception {

		new OrderBricksApplication().run(args);
	}
	
	private final HibernateBundle<OrderBricksServiceConfig> hibernate = new HibernateBundle<OrderBricksServiceConfig>(OrderDetailEntity.class) {

		public DataSourceFactory getDataSourceFactory(OrderBricksServiceConfig orderBricksServiceConfig) {
			return orderBricksServiceConfig.getDatabase();
		}
	};

	@Override
	public void initialize(Bootstrap<OrderBricksServiceConfig> bootstrap) {
		bootstrap.setConfigurationSourceProvider(new OrderBricksServiceConfigProvider());
		bootstrap.addBundle(hibernate);
		super.initialize(bootstrap);
	}

	@Override
	public void run(OrderBricksServiceConfig arg0, Environment environment) throws Exception {
		final OrderDAO dao = new OrderDAO(hibernate.getSessionFactory());
		environment.jersey().register(new OrderBricksResource(dao));
	}

	
}
