package com.order_bricks.service.resource;

import com.order_bricks.service.model.OrderBricksRequest;
import com.order_bricks.service.model.OrderBricksResponse;

public interface IOrderService {

	OrderBricksResponse saveOrder(OrderBricksRequest orderBricksRequest);
	OrderBricksResponse updateOrder(OrderBricksResponse orderBricksResponse);
	OrderBricksResponse getOrderDetails(int orderId);

}
