package com.order_bricks.service.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.order_bricks.service.entity.OrderDAO;
import com.order_bricks.service.model.Item;
import com.order_bricks.service.model.OrderBricksRequest;
import com.order_bricks.service.model.OrderBricksResponse;

import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.PATCH;
import io.swagger.annotations.ApiParam;

@Path("/orderdetails")
@Produces(MediaType.APPLICATION_JSON)
public class OrderBricksResource {

	private OrderDAO orderDAO;

	public OrderBricksResource(OrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}

	@Timed
	@GET
	@Produces(value = MediaType.APPLICATION_JSON)
	@Path("/healthcheck")
	public Response getPingStatus() {
		return Response.ok("{\"health\":\"running\"}").build();
	}

	@Timed
	@POST
	@UnitOfWork
	@Produces(value = MediaType.APPLICATION_JSON)
	@Path("/createOrder")
	public Response createOrder(
			@ApiParam(value = "Input Data", required = true) OrderBricksRequest orderBricksRequest) {

		IOrderService orderService = new OrderServiceImpl(orderDAO);
		OrderBricksResponse orderBricksResponse = orderService.saveOrder(orderBricksRequest);

		return Response.ok(orderBricksResponse).build();
	}

	@Timed
	@GET
	@UnitOfWork
	@Produces(value = MediaType.APPLICATION_JSON)
	@Path("/getOrder")
	public Response getOrderDetails(@QueryParam(value = "orderId") int orderId) {

		OrderServiceImpl orderServiceImpl = new OrderServiceImpl(orderDAO);
		OrderBricksResponse orderBricksResponse = orderServiceImpl.getOrderDetails(orderId);
		if (orderBricksResponse != null) {
			return Response.ok(orderBricksResponse).build();
		} else {
			throw new NotFoundException("Order ID Not Found");
		}
	}

	@Timed
	@PATCH
	@UnitOfWork
	@Produces(value = MediaType.APPLICATION_JSON)
	@Path("/updateOrder")
	public Response updateOrderDetails(
			@ApiParam(value = "Input Data", required = true) OrderBricksResponse orderBricksResponse) {

		OrderServiceImpl orderServiceImpl = new OrderServiceImpl(orderDAO);
		orderServiceImpl.updateOrder(orderBricksResponse);
		return Response.ok("{\"message\":\"OrderDetail Updated successfully\"}").build();
	}

}
