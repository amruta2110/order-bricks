package com.order_bricks.service.resource;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.BadRequestException;

import com.order_bricks.service.entity.OrderDAO;
import com.order_bricks.service.entity.OrderDetailEntity;
import com.order_bricks.service.model.OrderBricksRequest;
import com.order_bricks.service.model.OrderBricksResponse;

public class OrderServiceImpl implements IOrderService {

	private OrderDAO orderDAO;

	public OrderServiceImpl(OrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}

	public OrderBricksResponse saveOrder(OrderBricksRequest orderBricksRequest) {
		OrderDetailEntity orderDetailEntity = new OrderDetailEntity();
		orderDetailEntity.setOrderDate(getCovertedTimeStamp(orderBricksRequest.getOrderDate()));
		orderDetailEntity.setTotalAmount(orderBricksRequest.getTotalAmount());
		orderDetailEntity.setOrderBricksRequest(orderBricksRequest);
		orderDetailEntity.setStatus("ORDERED");
		orderDAO.createOrder(orderDetailEntity);
		return null;
	}

	private Timestamp getCovertedTimeStamp(String strDate) {
		Timestamp timestamp = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date parsedDate = dateFormat.parse(strDate);
			timestamp = new java.sql.Timestamp(parsedDate.getTime());
		} catch (Exception e) { // this generic but you can control another
								// types of exception
			// look the origin of excption
		}
		return timestamp;

	}

	public OrderBricksResponse updateOrder(OrderBricksResponse orderBricksResponse) {

		OrderDetailEntity orderDetailEntity = orderDAO.getOrderDetails(orderBricksResponse.getOrderId());

		if (orderDetailEntity.getStatus().equalsIgnoreCase("DELIVERED")
				|| (orderDetailEntity.getStatus().equalsIgnoreCase("DISPATCHED")
						&& orderBricksResponse.getStatus().equalsIgnoreCase("ORDERED"))) {
			throw new BadRequestException("Order is already " + orderDetailEntity.getStatus());

		} else {
			orderDetailEntity.setOrderId(orderBricksResponse.getOrderId());
			orderDetailEntity.setOrderDate(getCovertedTimeStamp(orderBricksResponse.getOrderDate()));
			orderDetailEntity.setTotalAmount(orderBricksResponse.getTotalAmount());

			OrderBricksRequest orderBricksRequest = orderDetailEntity.getOrderBricksRequest();
			orderBricksRequest.setItemList(orderBricksResponse.getItemList());
			orderBricksRequest.setOrderDate(orderBricksResponse.getOrderDate());
			orderBricksRequest.setTotalAmount(orderBricksResponse.getTotalAmount());
			orderDetailEntity.setOrderBricksRequest(orderBricksRequest);
			orderDetailEntity.setOrderDeliveryDate(getCovertedTimeStamp(orderBricksResponse.getOrderDeliveryDate()));
			orderDetailEntity
					.setOrderDispatchedDate(getCovertedTimeStamp(orderBricksResponse.getOrderDispatchedDate()));
			orderDetailEntity.setTransactionDate(getCovertedTimeStamp(orderBricksResponse.getTransactionDate()));
			orderDetailEntity.setStatus(orderBricksResponse.getStatus());
			orderDAO.updateOrder(orderDetailEntity);
		}

		return null;
	}

	public OrderBricksResponse getOrderDetails(int orderId) {

		OrderDetailEntity orderDetailEntity = orderDAO.getOrderDetails(orderId);
		if (orderDetailEntity != null) {
			OrderBricksResponse orderBricksResponse = new OrderBricksResponse();
			orderBricksResponse.setOrderId(orderDetailEntity.getOrderId());
			orderBricksResponse.setOrderDate(orderDetailEntity.getOrderDate() != null
					? getCovertedTimeStaptoString(orderDetailEntity.getOrderDate())
					: getCovertedTimeStaptoString(new Timestamp(System.currentTimeMillis())));
			orderBricksResponse.setOrderDeliveryDate(orderDetailEntity.getOrderDeliveryDate() != null
					? getCovertedTimeStaptoString(orderDetailEntity.getOrderDeliveryDate())
					: getCovertedTimeStaptoString(new Timestamp(System.currentTimeMillis())));
			orderBricksResponse.setOrderDispatchedDate(orderDetailEntity.getOrderDispatchedDate() != null
					? getCovertedTimeStaptoString(orderDetailEntity.getOrderDispatchedDate())
					: getCovertedTimeStaptoString(new Timestamp(System.currentTimeMillis())));
			orderBricksResponse.setTransactionDate(orderDetailEntity.getTransactionDate() != null
					? getCovertedTimeStaptoString(orderDetailEntity.getTransactionDate())
					: getCovertedTimeStaptoString(new Timestamp(System.currentTimeMillis())));
			orderBricksResponse.setTotalAmount(orderDetailEntity.getTotalAmount());
			orderBricksResponse.setItemList(orderDetailEntity.getOrderBricksRequest().getItemList());
			orderBricksResponse.setStatus(orderDetailEntity.getStatus());

			return orderBricksResponse;
		} else {
			return null;
		}
	}

	private String getCovertedTimeStaptoString(Timestamp date) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(date);
		return timeStamp;
	}

}
