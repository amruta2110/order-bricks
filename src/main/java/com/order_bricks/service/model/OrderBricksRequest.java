package com.order_bricks.service.model;

import java.util.ArrayList;
import java.util.List;

public class OrderBricksRequest {
	
	private String orderDate;
	private Double totalAmount;
    private List<Item> itemList = new ArrayList();
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public List<Item> getItemList() {
		return itemList;
	}
	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}

    
    

}
