package com.order_bricks.service.model;

public class Item {
		private String itemId;
		private String itemDesc;
        private String itemPrice;
		private String itemQty;
		public String getItemId() {
			return itemId;
		}
		public void setItemId(String itemId) {
			this.itemId = itemId;
		}
		public String getItemDesc() {
			return itemDesc;
		}
		public void setItemDesc(String itemDesc) {
			this.itemDesc = itemDesc;
		}
		public String getItemPrice() {
			return itemPrice;
		}
		public void setItemPrice(String itemPrice) {
			this.itemPrice = itemPrice;
		}
		public String getItemQty() {
			return itemQty;
		}
		public void setItemQty(String itemQty) {
			this.itemQty = itemQty;
		}


		   


}
