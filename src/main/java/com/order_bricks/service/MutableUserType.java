/**
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.order_bricks.service;

import java.io.Serializable;

import org.hibernate.type.SerializationException;
import org.hibernate.usertype.UserType;

/**
 * A skeleton Hibernate {@link UserType}. Assumes, by default, that the return
 * type is mutable. Subtypes whose {@code deepCopy} implementation returns a
 * non-serializable object <strong>must override</strong>
 * {@link #disassemble(Object)}.
 */
public abstract class MutableUserType implements UserType {

	public boolean isMutable() {
		return true;
	}

	public boolean equals(Object x, Object y) {
		if (x == y) {
			return true;
		}
		if ((x == null) || (y == null)) {
			return false;
		}
		return x.equals(y);
	}

	public int hashCode(Object x) {
		assert x != null;
		return x.hashCode();
	}

	public Object assemble(Serializable cached, Object owner) {
		// also safe for mutable objects
		return deepCopy(cached);
	}

	/**
	 * Disassembles the object in preparation for serialization. See
	 * {@link org.hibernate.usertype.UserType#disassemble(java.lang.Object)}.
	 * <p>
	 * Expects {@link #deepCopy(Object)} to return a {@code Serializable}.
	 * <strong>Subtypes whose {@code deepCopy} implementation returns a
	 * non-serializable object must override this method.</strong>
	 */
	public Serializable disassemble(Object value) {
		// also safe for mutable objects
		Object deepCopy = deepCopy(value);

		if (!(deepCopy instanceof Serializable)) {
			throw new SerializationException(String.format("deepCopy of %s is not serializable", value), null);
		}

		return (Serializable) deepCopy;
	}

	public Object replace(Object original, Object target, Object owner) {
		// also safe for mutable objects
		return deepCopy(original);
	}

}
