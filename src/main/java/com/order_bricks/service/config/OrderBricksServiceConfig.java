package com.order_bricks.service.config;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

public class OrderBricksServiceConfig extends Configuration {

	DataSourceFactory database = new DataSourceFactory();

	public DataSourceFactory getDatabase() {
		return database;
	}

	public void setDatabase(DataSourceFactory database) {
		this.database = database;
	}
	
}
