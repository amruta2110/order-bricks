package com.order_bricks.service.config;

import java.io.IOException;
import java.io.InputStream;

import io.dropwizard.configuration.ConfigurationSourceProvider;

public class OrderBricksServiceConfigProvider implements ConfigurationSourceProvider {

	public InputStream open(String arg0) throws IOException {
		return OrderBricksServiceConfigProvider.class.getClassLoader().getResourceAsStream("application.yml");
	}

}
